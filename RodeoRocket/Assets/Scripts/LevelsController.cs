﻿using UnityEngine;

public class LevelsController : MonoBehaviour
{
    [SerializeField] private LevelController[] _levelControllers;

    [SerializeField] private int _level;

    private void OnValidate()
    {
        _levelControllers = GetComponentsInChildren<LevelController>(true);
    }

    public void Init()
    {
        _level = 0;
        foreach (var levelController in _levelControllers)
            levelController.Init();
    }

    public bool StartLevel()
    {
        if (_level >= _levelControllers.Length) return false;
        
        foreach (var levelController in _levelControllers)
            levelController.Init();
        
        _levelControllers[_level++].StartLevel();
        return true;
    }

    public void RestartLevel()
    {
        _level--;
        StartLevel();
    }
}
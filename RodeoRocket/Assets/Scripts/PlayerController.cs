﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private Transform _leftArm, _rightArm, _centerPoint;
    [SerializeField] private LineRenderer _leftArmLine, _rightArmLine;
    [SerializeField] private Vector3 _startPosition;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private Camera _camera;
    [SerializeField] private float _force;

    [SerializeField] [HideInInspector] private int _targetLayer;

    private bool _ahead;
    [SerializeField] private bool _isInit;

    public bool Ahead
    {
        get { return _ahead; }
    }

    public Rigidbody2D Rigidbody2D
    {
        get { return _rigidbody2D; }
    }

    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _camera = FindObjectOfType<Camera>();
        _targetLayer = LayerMask.NameToLayer("Target");
    }

    public void Init()
    {
        _rigidbody2D.velocity = Vector2.zero;
        _rigidbody2D.rotation = 0;
        _rigidbody2D.angularVelocity = 0;
        _rigidbody2D.isKinematic = true;
        _rigidbody2D.position = _startPosition;
        _leftArmLine.SetPositions(new Vector3[2]);
        _rightArmLine.SetPositions(new Vector3[2]);
        DrawLines(_centerPoint.position);
        _isInit = true;
    }

    private void OnMouseDrag()
    {
        if (_isInit)
            DrawLines(_rigidbody2D.position = _camera.ScreenToWorldPoint(Input.mousePosition));
    }

    private void OnMouseUp()
    {
        if (_isInit)
            _rigidbody2D.isKinematic = false;
        var vector = _rigidbody2D.position - new Vector2(_centerPoint.position.x, _centerPoint.position.y);
        _rigidbody2D.AddForce(-vector * _force * Time.deltaTime, ForceMode2D.Impulse);
        DrawLines(_centerPoint.position);
    }

    private void DrawLines(Vector3 position)
    {
        _leftArmLine.SetPosition(0, _leftArm.position);
        _rightArmLine.SetPosition(0, _rightArm.position);
        _leftArmLine.SetPosition(1, position);
        _rightArmLine.SetPosition(1, position);
    }

    private void FixedUpdate()
    {
        if (_isInit)
            _ahead = _rigidbody2D.position.x > _centerPoint.position.x;
    }
}
﻿using UnityEngine;

public class StarController : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField] private float _mass;
    
    private void OnValidate()
    {
        _playerController = FindObjectOfType<PlayerController>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }
    
    private void FixedUpdate()
    {
        AttractPlayer();
    }

    public void AttractPlayer()
    {
        var rbToAttract = _playerController.Rigidbody2D;
        var direction = _rigidbody2D.position - rbToAttract.position;
        var distance = direction.magnitude;
        var forceMagnitude = _mass * rbToAttract.mass / Mathf.Pow(distance, 2);
        var force = direction.normalized * forceMagnitude;
        
        rbToAttract.AddForce(force);
    }
}
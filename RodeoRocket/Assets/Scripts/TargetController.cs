﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;

    [SerializeField] [HideInInspector] private int _playerTriggerLayer;

    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _playerTriggerLayer = LayerMask.NameToLayer("PlayerTrigger");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == _playerTriggerLayer) _gameController.NextLevel();
    }
}
﻿using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private CameraController _cameraController;

    private void OnValidate()
    {
        _playerController = FindObjectOfType<PlayerController>();
        _cameraController = FindObjectOfType<CameraController>();
    }

    public void Init()
    {
        gameObject.SetActive(false);
    }

    public void StartLevel()
    {
        gameObject.SetActive(true);
        _playerController.Init();
        _cameraController.Init();
    }
}
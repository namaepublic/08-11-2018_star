﻿using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private Camera _camera;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _zoomSmoothTime;
    [SerializeField] private float _zoomMax, _zoomMin;
    [SerializeField] [HideInInspector] private Vector3 _startPosition;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _playerController = FindObjectOfType<PlayerController>();
        _camera = GetComponent<Camera>();
        _startPosition = _playerController.transform.position + _offset;
    }

    private void Update()
    {
        if (EditorApplication.isPlaying) return;
        transform.position = _playerController.transform.position + _offset;
    }
#endif

    public void Init()
    {
        transform.position = _startPosition;
        _camera.orthographicSize = _zoomMax;
    }

    private void FixedUpdate()
    {
        if (_playerController.Rigidbody2D.isKinematic || !_playerController.Ahead) return;

        transform.position = _playerController.transform.position + _offset;
        Zoom();
    }

    private void Zoom()
    {
        var velocity = 0f;
        _camera.orthographicSize = Mathf.SmoothDamp(_camera.orthographicSize, _zoomMin, ref velocity, _zoomSmoothTime);
    }
}
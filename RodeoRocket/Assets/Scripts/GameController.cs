﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	[SerializeField] private LevelsController _levelsController;

	private void OnValidate()
	{
		_levelsController = FindObjectOfType<LevelsController>();
	}

	private void Start ()
	{
		_levelsController.Init();
		NextLevel();
	}

	public void NextLevel()
	{
		if (!_levelsController.StartLevel())
			GameOver();
	}

	public void RestartLevel()
	{
		_levelsController.RestartLevel();
	}
	
	public void GameOver()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
